FROM fluent/fluentd:v1.14-debian-1

USER root

RUN buildDeps="sudo make gcc g++ libc-dev" \
 && apt-get update \
 && apt-get install -y --no-install-recommends $buildDeps \
 && sudo gem install fluent-plugin-genhashvalue --version '~> 1' \
 && sudo gem install fluent-plugin-elasticsearch --version "5.0.5" \
 && sudo gem install fluent-plugin-gcloud-pubsub-custom  \
 && sudo gem install fluent-plugin-bigquery --version '2.3.0' \
 && sudo gem install fluent-plugin-grafana-loki --version '1.2.18' \
 && sudo gem install fluent-plugin-http-healthcheck --version '0.1.0' \
 && sudo gem install fluent-plugin-rewrite-tag-filter --version '~> 2.4'  \
 && sudo gem install fluent-plugin-prometheus --version '2.0.3' \
 && sudo gem install fluent-plugin-kafka --version '0.18.1' \
 && sudo gem install elasticsearch --version '7.14' \
 && sudo gem install elasticsearch-xpack --version '7.17.1' \
 && sudo gem uninstall elasticsearch --version '~> 8' \
 && sudo gem uninstall elasticsearch-api --version '~> 8' \
 && sudo gem uninstall elastic-transport --version '~> 8' \
 && sudo gem sources --clear-all \
 && SUDO_FORCE_REMOVE=yes \
    apt-get purge -y --auto-remove \
                  -o APT::AutoRemove::RecommendsImportant=false \
                  $buildDeps \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /tmp/* /var/tmp/* /usr/lib/ruby/gems/*/cache/*.gem

USER fluent
