.PHONY: build run upload

all: build

registry?=registry.gitlab.com/lucasdc6/fluentd-pubsub-elasticsearch
image_tag?=latest

build:
	docker build . $(docker_args) -t $(registry):$(image_tag)

run:
	docker run $(docker_args) $(registry):$(image_tag)

upload:
	docker push $(registry):$(image_tag)

