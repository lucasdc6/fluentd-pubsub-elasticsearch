# Test fluentd image

## TL;TR

```bash
# Run only fluentd
docker-compose up -d
# Run fluentd with monitoring
docker-compose -f docker-compose.yaml -f docker-compose-monitoring.yaml up -d
# Run logstash
docker-compose -f docker-compose-logstash.yaml up
# Run logstash with monitoring
docker-compose -f docker-compose-logstash.yaml -f docker-compose-monitoring.yaml up
```

## Configure Fluentd

All the fluentd configuration could be found at `config`, and its splitted in 4
sections, the main file config (`config/fluent.conf`), that include all the
directory `config/fluent.conf`. In that directory we could found 3 differents
files `00_system.conf`, `01_sources.conf` and `02_outputs.conf`

### system

On this file we define the general configuration, using the section `system`

### source

On this file we define all the sample data, the healthcheck plugin, the base
prometheus metrics, and we expose its

### outputs

On this file we define a filter stage that generate prometheus metrics about the
input records, then we match all the data and we rewrite the tags dynamically
and finally we generate the prometheus metrics related to the output records and
we show the records at `stdout`

## Run fluentd

To start the basic fluentd, run docker-compose

```bash
# start
docker-compose up -d
# stop
docker-compose stop
```

## Monitoring

To add the monitoring layer, we create a separated `docker-compose` file with
`prometheus` and `grafana` preconfigured to automatically scrape the fluentd
metrics (on the prometheus side), and show its on grafana with a preloaded
generic dashboard.

To start the monitoring stack, execute docker-compose with the
`docker-compose-monitoring.yaml` file

```bash
docker-compose -f docker-compose-monitoring.yaml up -d
```

You can check the basic monitoring on `http://localhost:3000/d/bNn5LUtizs3/fluentd-1-x`

