# fluentd pubsub

## Build image

This repository use GitlabCI to auto-build the images, if you want to build
locally the image, you could use the make task `build`

```bash
# With the default values
make build

# Specifying a tag
make build image_tag=1.0.0

# Specifying a tag and a registry
make build image_tag=1.0.0 registry=gitlab.com/my_registry
```

## Run image

To run the image, we have the task `run`

```bash
# Run image without extra config
make run

# Run the image with out custom fluentd config
make run docker_args="-v PATH_TO_CONFIG_FILES:/fluentd/etc"
```

## Publish image

```bash
# You need to be logged in into the registry
make upload
```

